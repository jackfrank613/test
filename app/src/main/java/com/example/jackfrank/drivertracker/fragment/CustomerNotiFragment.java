package com.example.jackfrank.drivertracker.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jackfrank.drivertracker.R;
import com.example.jackfrank.drivertracker.activity.CustomerMainActivity;
import com.example.jackfrank.drivertracker.adapter.CustomerNotiAdapter;
import com.example.jackfrank.drivertracker.adapter.CustomerNotificationAdapter;
import com.example.jackfrank.drivertracker.bottom_nav_service.BottomNavigationViewNew;
import com.example.jackfrank.drivertracker.model.CustomerNotiModel;
import com.example.jackfrank.drivertracker.utils.Constants;

import java.util.ArrayList;

public class CustomerNotiFragment extends Fragment {
    private ArrayList<CustomerNotiModel> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private CustomerNotificationAdapter mAdapter;
    private Constants constants;
    TextView logout;
    public static CustomerNotiFragment newInstance() {
        CustomerNotiFragment fragment = new CustomerNotiFragment();
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_customer_notification, container, false);
        initXml();
        recyclerView = (RecyclerView)view.findViewById(R.id.noti_recylerview);
        mAdapter = new CustomerNotificationAdapter (getActivity(),getListData());
        recyclerView .setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(mAdapter);

        return view;
    }
    public void initXml(){
        Toolbar toolbar = (Toolbar)getActivity().findViewById(R.id.toolbar);
        toolbar.setVisibility(View.VISIBLE);
        TextView title=(TextView)getActivity().findViewById(R.id.tvTitle);
        title.setText(getString(R.string.notifications));
        ImageView imageView=(ImageView)getActivity().findViewById(R.id.imagesearch);
        imageView.setVisibility(View.GONE);
        LinearLayout layout=(LinearLayout)getActivity().findViewById(R.id.tabbar);
        layout.setVisibility(View.GONE);
        BottomNavigationViewNew bottom=(BottomNavigationViewNew)getActivity().findViewById(R.id.navigation);
        bottom.setVisibility(View.VISIBLE);


    }

    @Override
    public void onResume() {
        super.onResume();
//        mAdapter = new CustomerNotiAdapter (getListData());
//        recyclerView.setAdapter(mAdapter);
    }

    private ArrayList<CustomerNotiModel> getListData() {
        movieList = new ArrayList<>();
        for (int i = 0; i <= 25; i++) {
            movieList.add(new CustomerNotiModel("You reached safely","4 days ago"));
        }
        return  movieList;
    }
}
