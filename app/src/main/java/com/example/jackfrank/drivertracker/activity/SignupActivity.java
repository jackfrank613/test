package com.example.jackfrank.drivertracker.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.jackfrank.drivertracker.R;
import com.example.jackfrank.drivertracker.utils.Constants;
import com.example.jackfrank.drivertracker.utils.PreferenceManager;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class SignupActivity extends AppCompatActivity {
    private static final String TAG = "Register";
    private  Button btn,sign_up;
    private TextView login;
    RadioButton radio_driver,radio_cutomer;
    Constants constants;
    private TextView txt_email,txt_name,txt_phone,txt_password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_signup);
//        Toast.makeText(SignupActivity.this,"test app",Toast.LENGTH_SHORT).show();
        constants=new Constants();
        constants.radio_select=true;
        initXml();
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radio);
        login=(TextView)findViewById(R.id.account);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                if(checkedId==R.id.radioDriver)
                {

                    constants.radio_select=true;
                }
                else {
                    constants.radio_select=false;
                }
            }
        });
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (constants.radio_select){

                    if(!txt_email.getText().toString().equals("")||!txt_password.getText().toString().equals("")||!txt_name.getText().toString().equals("")||!txt_phone.getText().toString().equals("")) {
                        DriverSingup();

                    }
                }
              else {

                    if(!txt_email.getText().toString().equals("")||!txt_password.getText().toString().equals("")||!txt_name.getText().toString().equals("")||!txt_phone.getText().toString().equals("")) {
                        CustomerSingup();

                    }

                }

            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(SignupActivity.this,LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }
    public void initXml(){
         btn=(Button)findViewById(R.id.next_btn);
         radio_driver = (RadioButton) findViewById(R.id.radioDriver);
         radio_cutomer = (RadioButton) findViewById(R.id.radioCustomer);
         txt_email=(TextView)findViewById(R.id.et_email);
         txt_name=(TextView)findViewById(R.id.et_name);
         txt_password=(TextView)findViewById(R.id.et_pass);
         txt_phone=(TextView)findViewById(R.id.et_phone);
    }

    private void DriverSingup(){
        String url="http://delivery.kavaltek.com/App/register_request";
      //  String urll= "http://kavaltek.com/api/User/register.php";
        String tag_usr="sign";
        HashMap<String,String> params=new HashMap<String,String>();
        params.put("name",txt_name.getText().toString());
        params.put("email",txt_email.getText().toString());
        params.put("phone",txt_phone.getText().toString());
        params.put("password",txt_password.getText().toString());
        params.put("auth","1");
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Signing...");
        pDialog.show();
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST,url,new JSONObject(params),
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Response", response.toString());
                        try {
                            String message=null;
                            if (response.getInt("status")==1)
                            {
                                pDialog.dismiss();
                                message=response.getString("message");
                                Toast.makeText(SignupActivity.this,message,Toast.LENGTH_SHORT).show();

//                                JSONObject initResult = response.getJSONObject("data");
                                String website="https://mail.google.com";
                                Intent brower=new Intent(Intent.ACTION_VIEW,Uri.parse(website));
                                startActivity(brower);
                                Intent intent = new Intent(SignupActivity.this, MainActivity.class);
                                startActivity(intent);
                            }
                            else {
                                pDialog.dismiss();
                                message=response.getString("message");
                                Toast.makeText(SignupActivity.this,message,Toast.LENGTH_SHORT).show();
                            }

                        }catch (JSONException e){}

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();
                        Log.d("Error.Response","that is not");
                    }
                }
        );
        // add the request object to the queue to be executed
        PreferenceManager.getInstance().addToRequestQueue(getRequest,tag_usr);
    }
    private void CustomerSingup(){
        String url="http://delivery.kavaltek.com/App/register_request";
        String tag_usr="sign";
        HashMap<String,String> params=new HashMap<String,String>();
        params.put("name",txt_name.getText().toString());
        params.put("email",txt_email.getText().toString());
        params.put("phone",txt_phone.getText().toString());
        params.put("password",txt_password.getText().toString());
        params.put("auth","0");
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Signing...");
        pDialog.show();
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST,url,new JSONObject(params),
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Response", response.toString());
                        try {
                            String message=null;
                            if (response.getInt("status")==1)
                            {
                                pDialog.dismiss();
                                message=response.getString("message");
                                Toast.makeText(SignupActivity.this,message,Toast.LENGTH_SHORT).show();
                                String userid=response.getString("user_id");
                                PreferenceManager.setCustomer(userid);
                                Intent intent = new Intent(SignupActivity.this, VerifyActivity.class);
                                startActivity(intent);
                            }
                            else {
                                pDialog.dismiss();
                                message=response.getString("message");
                                Toast.makeText(SignupActivity.this,message,Toast.LENGTH_SHORT).show();
                            }

                        }catch (JSONException e){}

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();
                        Log.d("Error.Response","that is not");
                    }
                }
        );
        // add the request object to the queue to be executed
        PreferenceManager.getInstance().addToRequestQueue(getRequest,tag_usr);
    }

}

