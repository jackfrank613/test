package com.example.jackfrank.drivertracker.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jackfrank.drivertracker.R;
import com.example.jackfrank.drivertracker.activity.CustomerMainActivity;
import com.example.jackfrank.drivertracker.fragment.CustomerHistoryFragment;
import com.example.jackfrank.drivertracker.fragment.CustomerOrderFragment;
import com.example.jackfrank.drivertracker.model.CustomerHomeSearchModel;
import com.example.jackfrank.drivertracker.model.CustomerNotiModel;
import com.example.jackfrank.drivertracker.model.PakageModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CustomerHomeSearchAdapter extends RecyclerView.Adapter<CustomerHomeSearchAdapter.MyViewHolder>{
    private List<PakageModel> moviesList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title,subtitle,distance;
        public ImageView p_image;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.store_title);
            subtitle= (TextView) view.findViewById(R.id.subtitle);
            p_image=(ImageView)view.findViewById(R.id.p_image);

        }
    }

    public CustomerHomeSearchAdapter(Context context, List<PakageModel> moviesList) {
        this.context=context;
        this.moviesList = moviesList;
    }
    @Override
    public CustomerHomeSearchAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_customer_search_gif, parent, false);
        return new CustomerHomeSearchAdapter.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(CustomerHomeSearchAdapter.MyViewHolder holder, int position) {
       PakageModel movie = moviesList.get(position);
        holder.title.setText(movie.getP_name());
        holder.subtitle.setText(movie.getP_location());
        Picasso.with(context).load(movie.getP_image()).placeholder(R.drawable.gif).into(holder.p_image);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context,"click listener",Toast.LENGTH_SHORT).show();
                fragmentJump();

            }
        });
    }
    public void fragmentJump(){
        CustomerOrderFragment orderFragment=CustomerOrderFragment.newInstance();
        switchContent(R.id.cus_content,orderFragment);

    }
    public void switchContent(int id, Fragment fragment) {
        if (context == null)
            return;
        if (context instanceof CustomerMainActivity) {
            CustomerMainActivity mainActivity = (CustomerMainActivity) context;
            Fragment frag = fragment;
            mainActivity.switchContent(id, frag);
        }
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
