package com.example.jackfrank.drivertracker.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.jackfrank.drivertracker.R;
import com.example.jackfrank.drivertracker.activity.LoginActivity;
import com.example.jackfrank.drivertracker.activity.SignupActivity;
import com.example.jackfrank.drivertracker.activity.VerifyActivity;
import com.example.jackfrank.drivertracker.utils.AppHelper;
import com.example.jackfrank.drivertracker.utils.PreferenceManager;
import com.example.jackfrank.drivertracker.utils.VolleyMultipartRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;

import static android.app.Activity.RESULT_OK;
import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;

public class CustomerPersonalInformation extends Fragment {

    private ImageView leftarrow, photo;
    private EditText et_place, et_home, et_work;
    private Button btn;
    private static final int CAMERA_REQUEST = 1888;
    private final int PICK_IMAGE_REQUEST = 71;
    private Uri filePath;

    public static CustomerPersonalInformation newInstance() {
        CustomerPersonalInformation fragment = new CustomerPersonalInformation();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_customer_personal_information, container, false);
        leftarrow = (ImageView) view.findViewById(R.id.arrow);
        photo = (ImageView) view.findViewById(R.id.photo);
        et_place = (EditText) view.findViewById(R.id.et_p);
        et_home = (EditText) view.findViewById(R.id.et_home);
        et_work = (EditText) view.findViewById(R.id.et_work);
        btn = (Button) view.findViewById(R.id.personal_btn);
        leftarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SignupActivity.class);
                startActivity(intent);

            }
        });
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!et_place.getText().toString().equals("")||!et_home.getText().toString().equals("")||!et_work.getText().toString().equals("")||!filePath.toString().equals(""))
                {
                    ReqestPersonal();

                }
                else {

                    Toast.makeText(getActivity(),"please,enter the correct values and photo",Toast.LENGTH_SHORT).show();
                }

            }
        });
        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
            filePath = data.getData();
            if (filePath != null) {

                photo.setImageURI(filePath);

            }
        }

    }
    public void ReqestPersonal(){
        String url="http://delivery.kavaltek.com/App/personal_info_customer";
        String tag="personal";
        final String user_id=PreferenceManager.getCustomer();
        VolleyMultipartRequest req = new VolleyMultipartRequest(Request.Method.POST,
                url, new Response.Listener<NetworkResponse>() {

            @Override
            public void onResponse(NetworkResponse response) {
                String responseStr=new String(response.data);
                try {
                    JSONObject responseobject=new JSONObject(responseStr);
                    if (responseobject.getInt("status")==1) {
                     String  message=responseobject.getString("message");
                        Toast.makeText(getActivity(),message,Toast.LENGTH_SHORT).show();
                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.content, CustomerVerifyFragment.newInstance()).addToBackStack("tag").commit();
                        try {
                            finalize();
                        } catch (Throwable throwable) {
                            throwable.printStackTrace();
                        }
                    }
                }catch (JSONException ignored){}
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d("Error.Response","that is not");
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                HashMap<String,String> params=new HashMap<String,String>();
                params.put("user_id",user_id);
                params.put("place",et_place.getText().toString());
                params.put("home",et_home.getText().toString());
                params.put("work",et_work.getText().toString());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                    params.put("image", new DataPart("customer.jpg", AppHelper.getFileDataFromDrawable(getActivity(),  photo.getDrawable()), "image/jpeg"));
                return params;
            }

        };
        // add the request object to the queue to be executed
        PreferenceManager.getInstance().addToRequestQueue(req,tag);
    }

    }

