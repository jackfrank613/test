package com.example.jackfrank.drivertracker.model;

public class CustomerHomeSearchModel {
    private String title;
    private String subtitle;
    private String distance;
    public CustomerHomeSearchModel(){}
    public CustomerHomeSearchModel(String title,String subtitle,String distance){
        this.title=title;
        this.subtitle=subtitle;
        this.distance=distance;
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }
}
