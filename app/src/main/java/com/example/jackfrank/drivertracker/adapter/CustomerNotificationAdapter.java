package com.example.jackfrank.drivertracker.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.jackfrank.drivertracker.R;
import com.example.jackfrank.drivertracker.activity.ChatActivity;
import com.example.jackfrank.drivertracker.activity.CustomerMainActivity;
import com.example.jackfrank.drivertracker.fragment.CustomerDriverProfileFragment;
import com.example.jackfrank.drivertracker.fragment.CustomerOrderFragment;
import com.example.jackfrank.drivertracker.model.CustomerNotiModel;

import java.util.List;

public class CustomerNotificationAdapter extends RecyclerView.Adapter<CustomerNotificationAdapter.MyViewHolder> {
    private List<CustomerNotiModel> moviesList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title,day;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.description);
            day = (TextView) view.findViewById(R.id.day);
        }
    }

    public CustomerNotificationAdapter(Context context, List<CustomerNotiModel> moviesList) {
        this.context=context;
        this.moviesList = moviesList;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_cutomer_notification, parent, false);
        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
       CustomerNotiModel movie = moviesList.get(position);
        holder.title.setText(movie.getTitle());
        holder.day.setText(movie.getDay());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context,"click listener",Toast.LENGTH_SHORT).show();

               fragmentJump();

            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
    public void fragmentJump(){
       CustomerDriverProfileFragment orderFragment=CustomerDriverProfileFragment.newInstance();
        switchContent(R.id.cus_content,orderFragment);

    }
    public void switchContent(int id, Fragment fragment) {
        if (context == null)
            return;
        if (context instanceof CustomerMainActivity) {
            CustomerMainActivity mainActivity = (CustomerMainActivity) context;
            Fragment frag = fragment;
            mainActivity.switchContent(id, frag);
        }
    }

}
