package com.example.jackfrank.drivertracker.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.example.jackfrank.drivertracker.R;
import com.example.jackfrank.drivertracker.activity.SignupActivity;

public class DriverAddVehicleDetailFragment extends Fragment {
    private Button btn_continue;
    public static DriverAddVehicleDetailFragment newInstance(){
        DriverAddVehicleDetailFragment fragment=new DriverAddVehicleDetailFragment();
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_vehicle_detail, container, false);
        ImageView leftarrow=(ImageView)view.findViewById(R.id.leftarrow);
        btn_continue=(Button)view.findViewById(R.id.btn_continue);
        leftarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction =getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.main_content, DriverVerifyMobileFragment.newInstance()).addToBackStack("tag").commit();
            }
        });
        btn_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction =getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.main_content, DriverSelectVehicleTypeFragment.newInstance()).addToBackStack("tag").commit();
            }
        });
        return view;
    }
}
