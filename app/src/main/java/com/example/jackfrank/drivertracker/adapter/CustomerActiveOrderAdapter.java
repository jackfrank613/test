package com.example.jackfrank.drivertracker.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.jackfrank.drivertracker.R;
import com.example.jackfrank.drivertracker.model.CustomerActiveOrderModel;
import com.example.jackfrank.drivertracker.model.CustomerHomeSearchModel;

import java.util.List;

public class CustomerActiveOrderAdapter extends RecyclerView.Adapter<CustomerActiveOrderAdapter.MyViewHolder> {
    private List<CustomerActiveOrderModel> moviesList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView d_name,d_price,pay_option,order_day,order_process,order_num;

        public MyViewHolder(View view) {
            super(view);
            d_name = (TextView) view.findViewById(R.id.d_name);
            d_price= (TextView) view.findViewById(R.id.price);
            order_day= (TextView) view.findViewById(R.id.o_day);
            order_process= (TextView) view.findViewById(R.id.o_process);
            order_num=(TextView)view.findViewById(R.id.o_num);
            pay_option=(TextView)view.findViewById(R.id.p_option);
        }
    }
    public CustomerActiveOrderAdapter(Context context, List<CustomerActiveOrderModel> moviesList) {
        this.context=context;
        this.moviesList = moviesList;
    }
    @Override
    public CustomerActiveOrderAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_customer_active, parent, false);
        return new CustomerActiveOrderAdapter.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(CustomerActiveOrderAdapter.MyViewHolder holder, int position) {
        CustomerActiveOrderModel movie = moviesList.get(position);
      holder.order_num.setText(movie.getD_number());
      holder.order_process.setText(movie.getOrder_process());
      holder.order_day.setText(movie.getOrder_day());
      holder.d_name.setText(movie.getD_name());
      holder.d_price.setText(movie.getD_price());
      holder.pay_option.setText(movie.getP_option());
    }
    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
