package com.example.jackfrank.drivertracker.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.jackfrank.drivertracker.R;
import com.example.jackfrank.drivertracker.adapter.CustomerHomeSearchAdapter;
import com.example.jackfrank.drivertracker.adapter.CustomerNotiAdapter;
import com.example.jackfrank.drivertracker.bottom_nav_service.BottomNavigationViewNew;
import com.example.jackfrank.drivertracker.model.CustomerHomeSearchModel;
import com.example.jackfrank.drivertracker.model.CustomerNotiModel;

import java.util.ArrayList;

public class CustomerSearchFragment extends Fragment {
    private ArrayList<CustomerHomeSearchModel> movieList;
    private RecyclerView recyclerView;
    private CustomerHomeSearchAdapter mAdapter;
    public static CustomerSearchFragment newInstance() {
        CustomerSearchFragment fragment = new CustomerSearchFragment();
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cutomer_home_search, container, false);
        initXml();
//        recyclerView = (RecyclerView)view.findViewById(R.id.search_recylerview);
//        mAdapter = new CustomerHomeSearchAdapter (getActivity(),getListData());
//        recyclerView .setLayoutManager(new LinearLayoutManager(getActivity()));
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
//        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
//        recyclerView.setAdapter(mAdapter);
        return view;
    }
    public void initXml(){
        Toolbar toolbar = (Toolbar)getActivity().findViewById(R.id.toolbar);
        toolbar.setVisibility(View.VISIBLE);
        ImageView imageView=(ImageView)getActivity().findViewById(R.id.imagesearch);
        imageView.setVisibility(View.GONE);
        LinearLayout layout=(LinearLayout)getActivity().findViewById(R.id.tabbar);
        layout.setVisibility(View.GONE);
        BottomNavigationViewNew bottom=(BottomNavigationViewNew)getActivity().findViewById(R.id.navigation);
        bottom.setVisibility(View.VISIBLE);
    }
    private ArrayList<CustomerHomeSearchModel> getListData() {
        movieList = new ArrayList<>();
        for (int i = 0; i <= 25; i++) {
            movieList.add(new CustomerHomeSearchModel("MRSOOL Package","8726 Prince Manur Ibn Abdulaiz,Al Ulaya,..","170km"));
        }
        return  movieList;
    }


}
