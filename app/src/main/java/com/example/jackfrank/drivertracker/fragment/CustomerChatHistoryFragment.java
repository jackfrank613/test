package com.example.jackfrank.drivertracker.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.jackfrank.drivertracker.R;
import com.example.jackfrank.drivertracker.adapter.ChatHistoryAdapter;
import com.example.jackfrank.drivertracker.adapter.CustomerChatHistoryAdapter;
import com.example.jackfrank.drivertracker.bottom_nav_service.BottomNavigationViewNew;
import com.example.jackfrank.drivertracker.model.ChatHistoryModel;
import com.example.jackfrank.drivertracker.utils.Constants;

import java.util.ArrayList;

public class CustomerChatHistoryFragment extends Fragment {

    private ArrayList<ChatHistoryModel> movieList;
    private RecyclerView recyclerView;
    private CustomerChatHistoryAdapter mAdapter;
    private Constants constants;

    public static CustomerChatHistoryFragment newInstance(){
        CustomerChatHistoryFragment fragment=new CustomerChatHistoryFragment();
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat_history, container, false);
        initXml();
        recyclerView = (RecyclerView)view.findViewById(R.id.history_recylerview);
        mAdapter = new CustomerChatHistoryAdapter(getActivity(),getListData());
        recyclerView .setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(mAdapter);
        return view;
    }
    public void initXml(){

        Toolbar toolbar = (Toolbar)getActivity().findViewById(R.id.toolbar);
        toolbar.setVisibility(View.VISIBLE);
        TextView title=(TextView)getActivity().findViewById(R.id.tvTitle);
        title.setText("Chat");
        LinearLayout layout=(LinearLayout)getActivity().findViewById(R.id.tabbar);
        layout.setVisibility(View.GONE);
        BottomNavigationViewNew bottomNavigationViewNew=(BottomNavigationViewNew)getActivity().findViewById(R.id.navigation);
        bottomNavigationViewNew.setVisibility(View.VISIBLE);
    }
    private ArrayList<ChatHistoryModel> getListData() {
        movieList = new ArrayList<>();
        for (int i = 0; i <= 25; i++) {
            movieList.add(new ChatHistoryModel("Ninja","thank you very much","09:28"));
        }
        return  movieList;
    }

}
