package com.example.jackfrank.drivertracker.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.jackfrank.drivertracker.R;
import com.example.jackfrank.drivertracker.adapter.CustomerActiveOrderAdapter;
import com.example.jackfrank.drivertracker.adapter.CustomerHomeSearchAdapter;
import com.example.jackfrank.drivertracker.bottom_nav_service.BottomNavigationViewNew;
import com.example.jackfrank.drivertracker.model.CustomerActiveOrderModel;
import com.example.jackfrank.drivertracker.model.CustomerHomeSearchModel;

import java.util.ArrayList;


public class CustomerActiveOrderFragment extends Fragment {

    private ArrayList<CustomerActiveOrderModel> movieList;
    private RecyclerView recyclerView;
    private CustomerActiveOrderAdapter mAdapter;

    public static CustomerActiveOrderFragment newInstance() {
        CustomerActiveOrderFragment fragment = new CustomerActiveOrderFragment();
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_customer_activeorder, container, false);
        initXml();
        recyclerView = (RecyclerView)view.findViewById(R.id.order_recylerview);
        mAdapter = new CustomerActiveOrderAdapter(getActivity(),getListData());
        recyclerView .setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(mAdapter);
        return view;
    }
    public void initXml(){
        Toolbar toolbar = (Toolbar)getActivity().findViewById(R.id.toolbar);
        toolbar.setVisibility(View.VISIBLE);
        TextView title=(TextView)getActivity().findViewById(R.id.tvTitle);
        title.setText("Order");
        LinearLayout layout=(LinearLayout)getActivity().findViewById(R.id.tabbar);
        layout.setVisibility(View.GONE);
        ImageView imageView=(ImageView)getActivity().findViewById(R.id.imagesearch);
        imageView.setVisibility(View.GONE);
        BottomNavigationViewNew bottom=(BottomNavigationViewNew)getActivity().findViewById(R.id.navigation);
        bottom.setVisibility(View.VISIBLE);

    }
    private ArrayList<CustomerActiveOrderModel> getListData() {
        movieList = new ArrayList<>();
        for (int i = 0; i <= 25; i++) {
            movieList.add(new CustomerActiveOrderModel("Aricson Smith","Order #042","114","Payment by Cash","Order on 11:45 am 01 Nov 2018","View Order"));
        }
        return  movieList;
    }
}
