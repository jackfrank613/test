package com.example.jackfrank.drivertracker.model;

public class ChatModel {
    private String message;
    private String chat_time;
    private String sort;
    private String img_url;
    private String txt_img;
    public ChatModel(){}
    public ChatModel(String message,String chat_time,String sort,String img_url){
        this.chat_time=chat_time;
        this.message=message;
        this.sort=sort;
        this.img_url=img_url;
        this.txt_img=txt_img;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getChat_time() {
        return chat_time;
    }

    public void setChat_time(String chat_time) {
        this.chat_time = chat_time;
    }
    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }
    public String getTxt_img() {
        return txt_img;
    }
    public void setTxt_img(String txt_img) {
        this.txt_img = txt_img;
    }
}
