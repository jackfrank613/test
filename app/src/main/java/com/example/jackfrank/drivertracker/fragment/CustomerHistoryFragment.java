package com.example.jackfrank.drivertracker.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.jackfrank.drivertracker.R;
import com.example.jackfrank.drivertracker.adapter.CustomerHomeSearchAdapter;
import com.example.jackfrank.drivertracker.bottom_nav_service.BottomNavigationViewNew;
import com.example.jackfrank.drivertracker.model.CustomerHomeSearchModel;

import java.util.ArrayList;

public class CustomerHistoryFragment extends Fragment {

    private ArrayList<CustomerHomeSearchModel> movieList;
    private RecyclerView recyclerView;
    private CustomerHomeSearchAdapter mAdapter;
    private Button button,his_btn,nearby_btn;
    public static CustomerHistoryFragment newInstance() {
        CustomerHistoryFragment fragment = new CustomerHistoryFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_customer_home, container, false);
        initXml();
        recyclerView = (RecyclerView)view.findViewById(R.id.home_recylerview);
//        mAdapter = new CustomerHomeSearchAdapter(getActivity(),getListData());
//        recyclerView .setLayoutManager(new LinearLayoutManager(getActivity()));
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
//        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
//        recyclerView.setAdapter(mAdapter);
        return view;
    }
    public void initXml(){

        Toolbar toolbar = (Toolbar)getActivity().findViewById(R.id.toolbar);
        toolbar.setVisibility(View.VISIBLE);
        TextView title=(TextView)getActivity().findViewById(R.id.tvTitle);
        title.setText("HOME");
        LinearLayout layout=(LinearLayout)getActivity().findViewById(R.id.tabbar);
        layout.setVisibility(View.VISIBLE);
        ImageView imageView=(ImageView)getActivity().findViewById(R.id.imagesearch);
        imageView.setVisibility(View.VISIBLE);
        button=(Button)getActivity().findViewById(R.id.all_btn);
        button.setBackgroundResource(R.drawable.roundedbutton);
        his_btn=(Button)getActivity().findViewById(R.id.history_btn);
        his_btn.setBackgroundResource(R.drawable.roundedbutton_pressed);
        nearby_btn=(Button)getActivity().findViewById(R.id.see_btn);
        nearby_btn.setBackgroundResource(R.drawable.roundedbutton);
        BottomNavigationViewNew bottom=(BottomNavigationViewNew)getActivity().findViewById(R.id.navigation);
        bottom.setVisibility(View.VISIBLE);

    }
    private ArrayList<CustomerHomeSearchModel> getListData() {
        movieList = new ArrayList<>();
        for (int i = 0; i <= 25; i++) {
            movieList.add(new CustomerHomeSearchModel("MRSOOL Package","8726 Prince Manur Ibn Abdulaiz,Al Ulaya,..","170km"));
        }
        return  movieList;
    }

}
