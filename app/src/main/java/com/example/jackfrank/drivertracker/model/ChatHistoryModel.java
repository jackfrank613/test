package com.example.jackfrank.drivertracker.model;

public class ChatHistoryModel {

    private String name;
    private String last_message;
    private String time;

    public ChatHistoryModel(){}
    public ChatHistoryModel(String name,String last_message,String time){
        this.name=name;
        this.last_message=last_message;
        this.time=time;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLast_message() {
        return last_message;
    }

    public void setLast_message(String last_message) {
        this.last_message = last_message;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }


}
