package com.example.jackfrank.drivertracker.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.jackfrank.drivertracker.R;
import com.example.jackfrank.drivertracker.utils.Constants;
import com.example.jackfrank.drivertracker.utils.GPSTracker;
import com.example.jackfrank.drivertracker.utils.PreferenceManager;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class LoginActivity extends AppCompatActivity {
    private Button btn;
    private Constants constants;
    private TextView txt_email,txt_password;
    String refreshedToken;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        constants=new Constants();
        constants.login_select=true;
        refreshedToken = FirebaseInstanceId.getInstance().getToken();
        setContentView(R.layout.fragment_logain);
        initXml();
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radio);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                if(checkedId==R.id.radioDriver)
                {
                    constants.login_select=true;
                }
                else {
                    constants.login_select=false;
                }
            }
        });
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (constants.login_select){
                    DriverLogin();

                }
                else {
                    CustomerLogin();

                }
            }
        });

    }
    public void initXml(){
        btn=(Button)findViewById(R.id.log_btn);
        txt_email=(TextView)findViewById(R.id.et_email);
        txt_password=(TextView)findViewById(R.id.et_pass);
    }
    public void DriverLogin(){
        String url="http://delivery.kavaltek.com/App/login_request";
        String tag_usr="login";
        HashMap<String,String> params=new HashMap<String,String>();
        params.put("user",txt_email.getText().toString());
        params.put("password",txt_password.getText().toString());
        params.put("token",refreshedToken);
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Login...");
        pDialog.show();
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST,url,new JSONObject(params),
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Response", response.toString());
                        try {
                            String message=null;
                            if (response.getInt("status")==1)
                            {
                                pDialog.dismiss();
                                message=response.getString("message");
                                Toast.makeText(LoginActivity.this,message,Toast.LENGTH_SHORT).show();

//                                JSONObject initResult = response.getJSONObject("data");
                                Intent intent = new Intent(LoginActivity.this, DriverMainActivity.class);
                                startActivity(intent);
                            }
                            else {
                                pDialog.dismiss();
                                message=response.getString("message");
                                Toast.makeText(LoginActivity.this,message,Toast.LENGTH_SHORT).show();
                            }

                        }catch (JSONException e){}

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();
                        Log.d("Error.Response","that is not");
                    }
                }
        );
        // add the request object to the queue to be executed
        PreferenceManager.getInstance().addToRequestQueue(getRequest,tag_usr);
    }

    public void CustomerLogin(){

        String url="http://delivery.kavaltek.com/App/login_request";
        String tag_usr="login";
        HashMap<String,String> params=new HashMap<String,String>();
        params.put("user",txt_email.getText().toString());
        params.put("password",txt_password.getText().toString());
        params.put("token",refreshedToken);
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Login...");
        pDialog.show();
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST,url,new JSONObject(params),
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Response", response.toString());
                        try {
                            String message=null;
                            if (response.getInt("status")==1)
                            {
                                pDialog.dismiss();
                                message=response.getString("message");
                                Toast.makeText(LoginActivity.this,message,Toast.LENGTH_SHORT).show();

//                                JSONObject initResult = response.getJSONObject("data");
                                Intent intent=new Intent(LoginActivity.this,CustomerMainActivity.class);
                                startActivity(intent);
                            }
                            else {
                                pDialog.dismiss();
                                message=response.getString("message");
                                Toast.makeText(LoginActivity.this,message,Toast.LENGTH_SHORT).show();
                            }

                        }catch (JSONException e){}

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();
                        Log.d("Error.Response","that is not");
                    }
                }
        );
        // add the request object to the queue to be executed
        PreferenceManager.getInstance().addToRequestQueue(getRequest,tag_usr);


    }
}
