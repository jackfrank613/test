package com.example.jackfrank.drivertracker.activity;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.jackfrank.drivertracker.R;
import com.example.jackfrank.drivertracker.fragment.CustomerNearbyFragment;
import com.example.jackfrank.drivertracker.fragment.DriverPersonalInformation;
import com.example.jackfrank.drivertracker.fragment.DriverVerifyEmailFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main_content, DriverPersonalInformation.newInstance()).addToBackStack("tag").commit();
    }
}
