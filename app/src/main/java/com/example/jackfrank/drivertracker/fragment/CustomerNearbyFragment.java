package com.example.jackfrank.drivertracker.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.jackfrank.drivertracker.R;
import com.example.jackfrank.drivertracker.adapter.CustomerHomeSearchAdapter;
import com.example.jackfrank.drivertracker.bottom_nav_service.BottomNavigationViewNew;
import com.example.jackfrank.drivertracker.model.CustomerHomeSearchModel;
import com.example.jackfrank.drivertracker.model.PakageModel;
import com.example.jackfrank.drivertracker.utils.GPSTracker;
import com.example.jackfrank.drivertracker.utils.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class CustomerNearbyFragment extends Fragment {
    private ArrayList<PakageModel> movieList;
    private RecyclerView recyclerView;
    private CustomerHomeSearchAdapter mAdapter;
    private Button button,his_btn,nearby_btn;
    private double latitude,longitude;
    public static CustomerNearbyFragment newInstance() {
        CustomerNearbyFragment fragment = new CustomerNearbyFragment();
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_customer_seenearby, container, false);
        GPSTracker gps=new GPSTracker(getActivity());
        // gps.showSettingsAlert();
        latitude = gps.getLatitude();
        longitude = gps.getLongitude();
        initXml();
        recyclerView = (RecyclerView)view.findViewById(R.id.nearby_recylerview);
        initReadpackage();
        return view;
    }
    public void initXml(){

        Toolbar toolbar = (Toolbar)getActivity().findViewById(R.id.toolbar);
        toolbar.setVisibility(View.VISIBLE);
        TextView title=(TextView)getActivity().findViewById(R.id.tvTitle);
        title.setText("HOME");
        LinearLayout layout=(LinearLayout)getActivity().findViewById(R.id.tabbar);
        layout.setVisibility(View.VISIBLE);
        ImageView imageView=(ImageView)getActivity().findViewById(R.id.imagesearch);
        imageView.setVisibility(View.VISIBLE);
        button=(Button)getActivity().findViewById(R.id.all_btn);
        button.setBackgroundResource(R.drawable.roundedbutton);
        his_btn=(Button)getActivity().findViewById(R.id.history_btn);
        his_btn.setBackgroundResource(R.drawable.roundedbutton);
        nearby_btn=(Button)getActivity().findViewById(R.id.see_btn);
        nearby_btn.setBackgroundResource(R.drawable.roundedbutton_pressed);
        BottomNavigationViewNew bottom=(BottomNavigationViewNew)getActivity().findViewById(R.id.navigation);
        bottom.setVisibility(View.VISIBLE);

    }
    public void initReadpackage(){
        String url="http://delivery.kavaltek.com/App/near_by";
        String tag_usr="package";
        HashMap<String,String> params=new HashMap<String,String>();
        params.put("lat",String.valueOf(latitude));
        params.put("lng",String.valueOf(longitude));
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST,url,new JSONObject(params),
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Response", response.toString());
                        movieList = new ArrayList<>();
                        try {
                            if (response.getInt("status")==1)
                            {
                                JSONArray array=response.getJSONArray("data");
                                if(array!=null)
                                {
                                    for(int i=0;i<array.length();i++)
                                    {
                                        JSONObject pakage=array.getJSONObject(i);
                                        String id=pakage.getString("package_id");
                                        String p_name=pakage.getString("name");
                                        String p_location=pakage.getString("location");
                                        String p_image=pakage.getString("image");
                                        PakageModel pakageModel=new PakageModel(id,p_name,p_location,p_image);
                                        movieList.add(pakageModel);
                                    }
                                    mAdapter = new CustomerHomeSearchAdapter (getActivity(),movieList);
                                    recyclerView .setLayoutManager(new LinearLayoutManager(getActivity()));
                                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                                    recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
                                    recyclerView.setAdapter(mAdapter);

                                }
                            }
                            else {

                                Toast.makeText(getActivity(),"error",Toast.LENGTH_SHORT).show();
                            }

                        }catch (JSONException e){}

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response","that is not");
                    }
                }
        );
        // add the request object to the queue to be executed
        PreferenceManager.getInstance().addToRequestQueue(getRequest,tag_usr);
    }
}
