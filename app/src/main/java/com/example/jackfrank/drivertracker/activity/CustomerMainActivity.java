package com.example.jackfrank.drivertracker.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.jackfrank.drivertracker.R;
import com.example.jackfrank.drivertracker.bottom_nav_service.BottomNavigationViewNew;
import com.example.jackfrank.drivertracker.fragment.ChatHistoryFragment;
import com.example.jackfrank.drivertracker.fragment.CustomerActiveOrderFragment;
import com.example.jackfrank.drivertracker.fragment.CustomerChatHistoryFragment;
import com.example.jackfrank.drivertracker.fragment.CustomerHistoryFragment;
import com.example.jackfrank.drivertracker.fragment.CustomerHomeFragment;
import com.example.jackfrank.drivertracker.fragment.CustomerNearbyFragment;
import com.example.jackfrank.drivertracker.fragment.CustomerNotiFragment;
import com.example.jackfrank.drivertracker.fragment.CustomerOrderFragment;
import com.example.jackfrank.drivertracker.fragment.CustomerProfileFragment;
import com.example.jackfrank.drivertracker.fragment.CustomerSearchFragment;
import com.example.jackfrank.drivertracker.fragment.TimePickerFragment;
import com.example.jackfrank.drivertracker.utils.PreferenceManager;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;


public class CustomerMainActivity extends AppCompatActivity{

    private String TAG="customermainactivity";
    private DrawerLayout mDrawerLayout;
    Toolbar toolbar;
    android.support.v7.app.ActionBarDrawerToggle mDrawerToggle;
    private Button button;
    private ImageView s_image;
    private Button his_btn,nearby_btn;
    private LinearLayout log_layout;

    private BottomNavigationViewNew.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationViewNew.OnNavigationItemSelectedListener() {
        @SuppressLint({"ResourceAsColor", "SetTextI18n", "NewApi"})
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment = null;
            switch (item.getItemId()) {
                case R.id.home_bottom:
                    selectedFragment = CustomerHomeFragment.newInstance();
                    break;
                case R.id.order_bottom:
                 selectedFragment = CustomerActiveOrderFragment.newInstance();

                    break;
                case R.id.chat_bottom:
                    selectedFragment = CustomerChatHistoryFragment.newInstance();
                    break;
                case R.id.profile_bottom:
                    selectedFragment = CustomerProfileFragment.newInstance();
                    break;

                case R.id.noti_bottom:
                    selectedFragment = CustomerNotiFragment.newInstance();

                    break;
                default:

            }
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            assert selectedFragment != null;
            transaction.replace(R.id.cus_content, selectedFragment).addToBackStack("tag").commit();
            return true;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customer_main_activity);
        BottomNavigationViewNew bnve = (BottomNavigationViewNew) findViewById(R.id.navigation);
        bnve.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        initXml();
        //first display
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.cus_content, CustomerHomeFragment.newInstance()).addToBackStack("tag").commit();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        setupToolbar();
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        setupDrawerToggle();
        log_layout=(LinearLayout)findViewById(R.id.logout);
        log_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(CustomerMainActivity.this,"logout",Toast.LENGTH_SHORT).show();
            }
        });
        clikListener();
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }
    void setupToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");
    }
    void setupDrawerToggle(){
        mDrawerToggle = new android.support.v7.app.ActionBarDrawerToggle(this,mDrawerLayout,toolbar,R.string.app_name, R.string.app_name);
        //This is necessary to change the icon of the Drawer Toggle upon state change.
        mDrawerToggle.syncState();
    }
    public void initXml(){
        button=(Button)findViewById(R.id.all_btn);
        button.setBackgroundResource(R.drawable.roundedbutton_pressed);
        his_btn=(Button)findViewById(R.id.history_btn);
        s_image=(ImageView)findViewById(R.id.imagesearch);
        nearby_btn=(Button)findViewById(R.id.see_btn);
        log_layout=(LinearLayout)findViewById(R.id.logout);

    }
    public void clikListener(){

        s_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.cus_content, CustomerSearchFragment.newInstance()).addToBackStack("tag").commit();
            }
        });
        his_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.cus_content, CustomerHistoryFragment.newInstance()).addToBackStack("tag").commit();
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.cus_content, CustomerHomeFragment.newInstance()).addToBackStack("tag").commit();
            }
        });
       nearby_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.cus_content, CustomerNearbyFragment.newInstance()).addToBackStack("tag").commit();
            }
        });
        log_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(CustomerMainActivity.this);
                builder.setTitle(R.string.app_name);
                builder.setMessage("Do you want to exit?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finishAffinity();
                                System.exit(0);

                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }
    public void switchContent(int id, Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(id, fragment, fragment.toString());
        ft.addToBackStack(null);
        ft.commit();
    }


}

