package com.example.jackfrank.drivertracker.fragment;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.example.jackfrank.drivertracker.R;
import com.example.jackfrank.drivertracker.activity.SignupActivity;

public class DriverPersonalInformation extends Fragment {

    private Button btn_continue;

    public static DriverPersonalInformation newInstance(){

        DriverPersonalInformation fragment=new DriverPersonalInformation();

        return fragment;

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_driver_personal_information, container, false);
        ImageView leftarrow=(ImageView)view.findViewById(R.id.arrow);
        btn_continue=(Button)view.findViewById(R.id.personal_btn);
        leftarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(),SignupActivity.class);
                startActivity(intent);
            }
        });
        btn_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction =getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.main_content, DriverVerifyEmailFragment.newInstance()).addToBackStack("tag").commit();
            }
        });
          return view;
    }

}
