package com.example.jackfrank.drivertracker.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.jackfrank.drivertracker.R;
import com.example.jackfrank.drivertracker.bottom_nav_service.BottomNavigationViewNew;

public class DriverCustomerRateNow extends Fragment {

    public static DriverCustomerRateNow newInstance(){
        DriverCustomerRateNow fragment=new DriverCustomerRateNow();
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.customer_rate_now, container, false);
        initXml();
        ImageView left_arrow=(ImageView)view.findViewById(R.id.left_arrow);
        left_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.cus_content, DriverCustomerProfileFragment.newInstance()).addToBackStack("tag").commit();
            }
        });
        return view;
    }
    public void  initXml(){
        Toolbar toolbar = (Toolbar)getActivity().findViewById(R.id.toolbar);
        toolbar.setVisibility(View.GONE);
        LinearLayout layout=(LinearLayout)getActivity().findViewById(R.id.tabbar);
        layout.setVisibility(View.GONE);
        BottomNavigationViewNew bottom=(BottomNavigationViewNew)getActivity().findViewById(R.id.navigation);
        bottom.setVisibility(View.GONE);
    }


}
