package com.example.jackfrank.drivertracker.model;

public class CustomerNotiModel {

    private String title, day;
    public CustomerNotiModel() {
    }
    public CustomerNotiModel(String title,String day) {
        this.title=title;
        this.day=day;
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }
}
