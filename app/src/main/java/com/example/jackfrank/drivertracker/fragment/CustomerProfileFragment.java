package com.example.jackfrank.drivertracker.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.jackfrank.drivertracker.R;
import com.example.jackfrank.drivertracker.bottom_nav_service.BottomNavigationViewNew;
import com.example.jackfrank.drivertracker.model.CustomerProfileModel;
import com.example.jackfrank.drivertracker.utils.AppHelper;
import com.example.jackfrank.drivertracker.utils.Constants;
import com.example.jackfrank.drivertracker.utils.PreferenceManager;
import com.example.jackfrank.drivertracker.utils.VolleyMultipartRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class CustomerProfileFragment extends Fragment {
    private CustomerProfileModel customerProfileModel;
    private EditText txt_name,txt_email,txt_phone,txt_plce,txt_home,txt_work;
    private Button sendbtn;
    private ImageView cus_photo;
    private final int PICK_IMAGE_REQUEST = 71;
    private Uri filePath;
    private Constants constants;

    public static CustomerProfileFragment newInstance() {
        CustomerProfileFragment fragment = new CustomerProfileFragment();
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_customer_profile, container, false);
        constants=new Constants();
        initXml();
        txt_name=(EditText)view.findViewById(R.id.u_name);
        txt_email=(EditText)view.findViewById(R.id.u_email);
        txt_phone=(EditText)view.findViewById(R.id.u_cellar);
        txt_plce=(EditText)view.findViewById(R.id.place);
        txt_home=(EditText)view.findViewById(R.id.u_home);
        txt_work=(EditText)view.findViewById(R.id.u_work);
        cus_photo=(ImageView)view.findViewById(R.id.cus_image);
        sendbtn=(Button)view.findViewById(R.id.update);
        initCustomerProfile();
        cus_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                constants.img=true;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });
        sendbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!txt_phone.getText().toString().equals("")||!txt_name.getText().toString().equals("")||!txt_email.getText().toString().equals("")||!txt_plce.getText().toString().equals("")||!txt_home.getText().toString().equals("")||!txt_work.getText().toString().equals(""))
                {
                    updateProfile();
                }
                else {

                    Toast.makeText(getActivity(),"please,enter the correct values and image",Toast.LENGTH_SHORT).show();
                }

            }
        });
        return view;
    }
    public void initXml(){
        Toolbar toolbar=(Toolbar)getActivity().findViewById(R.id.toolbar);
        toolbar.setVisibility(View.VISIBLE);
        LinearLayout layout=(LinearLayout)getActivity().findViewById(R.id.tabbar);
        layout.setVisibility(View.GONE);
        TextView title=(TextView)getActivity().findViewById(R.id.tvTitle);
        title.setText("Profile");
        ImageView imageView=(ImageView)getActivity().findViewById(R.id.imagesearch);
        imageView.setVisibility(View.GONE);
        BottomNavigationViewNew bottom=(BottomNavigationViewNew)getActivity().findViewById(R.id.navigation);
        bottom.setVisibility(View.VISIBLE);
    }
    public void initCustomerProfile(){
        String url="http://delivery.kavaltek.com/App/edit_profile";
        String tag="profile";
        String user_id=PreferenceManager.getCustomer();
        HashMap<String,String> params=new HashMap<String,String>();
        params.put("user_id",user_id);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST,url,new JSONObject(params),
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Response", response.toString());
                        try {
                            if (response.getInt("status")==1)
                            {
                                JSONObject object=response.getJSONObject("data");
                                String user_id=object.getString("user_id");
                                String name=object.getString("name");
                                String email=object.getString("email");
                                String token=object.getString("token");
                                String email_verify=object.getString("email_verify_code");
                                String email_verify_status=object.getString("email_verify_status");
                                String auth=object.getString("auth");
                                String phone=object.getString("phone");
                                String phone_verify_code=object.getString("phone_verify_code");
                                String phone_verify=object.getString("phone_verify_status");
                                String password=object.getString("password");
                                String register_date=object.getString("register_date");
                                String customer_id=object.getString("customer_id");
                                String place=object.getString("place");
                                String image=object.getString("image");
                                String home=object.getString("home");
                                String work=object.getString("work");
                                customerProfileModel=new CustomerProfileModel(user_id,name,email,token,email_verify,email_verify_status,auth,phone,phone_verify,password,register_date,customer_id,place,image,home,work,phone_verify_code);
                                txt_name.setText(name);
                                txt_email.setText(email);
                                txt_phone.setText(phone);
                                txt_plce.setText(place);
                                txt_home.setText(home);
                                txt_work.setText(work);
                                Picasso.with(getActivity()).load(image).placeholder(R.drawable.driver).into(cus_photo);

                            }
                            else {

                               Toast.makeText(getActivity(),"error",Toast.LENGTH_SHORT).show();

                            }

                        }catch (JSONException e){}

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response","that is not");
                    }
                }
        );
        // add the request object to the queue to be executed
        PreferenceManager.getInstance().addToRequestQueue(getRequest,tag);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
            filePath = data.getData();
            if (filePath != null) {

                cus_photo.setImageURI(filePath);

            }
        }

    }

    public void updateProfile(){
             String url="http://delivery.kavaltek.com/App/update_profile";
             String tag="update";
             if(constants.img){
                 final ProgressDialog pDialog = new ProgressDialog(getActivity());
                 pDialog.setMessage("Updating...");
                 pDialog.show();
                 VolleyMultipartRequest req = new VolleyMultipartRequest(Request.Method.POST,
                         url, new Response.Listener<NetworkResponse>() {

                     @Override
                     public void onResponse(NetworkResponse response) {
                         String responseStr=new String(response.data);
                         try {
                             JSONObject responseobject=new JSONObject(responseStr);
                             if (responseobject.getInt("status")==1) {
                                 pDialog.dismiss();
                                 String  message=responseobject.getString("message");
                                 Toast.makeText(getActivity(),message,Toast.LENGTH_SHORT).show();
                                 try {
                                     finalize();
                                 } catch (Throwable throwable) {
                                     throwable.printStackTrace();
                                 }
                             }
                         }catch (JSONException ignored){}
                     }
                 }, new Response.ErrorListener() {

                     @Override
                     public void onErrorResponse(VolleyError error) {
                         pDialog.dismiss();
                         Log.d("Error.Response","that is not");
                     }
                 }) {

                     @Override
                     protected Map<String, String> getParams() {
                         // Posting params to register url
                         HashMap<String,String> params=new HashMap<String,String>();
                         params.put("user_id",customerProfileModel.getUser_id());
                         params.put("name",customerProfileModel.getName());
                         params.put("email",customerProfileModel.getEmail());
                         params.put("token",customerProfileModel.getToken());
                         params.put("email_verify_code",customerProfileModel.getEmail_verify_code());
                         params.put("email_verify_status",customerProfileModel.getEmail_verify_status());
                         params.put("auth",customerProfileModel.getAuth());
                         params.put("phone",customerProfileModel.getPhone());
                         params.put("phone_verify_status",customerProfileModel.getPhone_verify());
                         params.put("phone_verify_code",customerProfileModel.getPhone_verify_code());
                         params.put("password",customerProfileModel.getPassword());
                         params.put("register_date",customerProfileModel.getPassword());
                         params.put("customer_id",customerProfileModel.getCustomer_id());
                         params.put("place",customerProfileModel.getPlace());
                         params.put("image",customerProfileModel.getImage());
                         params.put("home",customerProfileModel.getHome());
                         params.put("work",customerProfileModel.getWork());
                         return params;
                     }

                     @Override
                     protected Map<String, DataPart> getByteData() {
                         Map<String, DataPart> params = new HashMap<>();
                         // file name could found file base or direct access from real path
                         // for now just get bitmap data from ImageView
                         //params.put("image", new DataPart("customer.jpg", AppHelper.getFileDataFromDrawable(getActivity(),  cus_photo.getDrawable()), "image/jpeg"));
                         return params;
                     }

                 };
                 // add the request object to the queue to be executed
                 PreferenceManager.getInstance().addToRequestQueue(req,tag);

             }else {

                 final ProgressDialog pDialog = new ProgressDialog(getActivity());
                 pDialog.setMessage("Updating...");
                 pDialog.show();
                 VolleyMultipartRequest req = new VolleyMultipartRequest(Request.Method.POST,
                         url, new Response.Listener<NetworkResponse>() {

                     @Override
                     public void onResponse(NetworkResponse response) {
                         String responseStr=new String(response.data);
                         try {
                             JSONObject responseobject=new JSONObject(responseStr);
                             if (responseobject.getInt("status")==1) {
                                 pDialog.dismiss();
                                 String  message=responseobject.getString("message");
                                 Toast.makeText(getActivity(),message,Toast.LENGTH_SHORT).show();
                                 try {
                                     finalize();
                                 } catch (Throwable throwable) {
                                     throwable.printStackTrace();
                                 }
                             }
                         }catch (JSONException ignored){}
                     }
                 }, new Response.ErrorListener() {

                     @Override
                     public void onErrorResponse(VolleyError error) {
                         pDialog.dismiss();
                         Log.d("Error.Response","that is not");
                     }
                 }) {


                     @Override
                     protected Map<String, String> getParams() {
                         // Posting params to register url
                         HashMap<String,String> params=new HashMap<String,String>();
                         params.put("user_id",customerProfileModel.getUser_id());
                         params.put("name",customerProfileModel.getName());
                         params.put("email",customerProfileModel.getEmail());
                         params.put("token",customerProfileModel.getToken());
                         params.put("email_verify_code",customerProfileModel.getEmail_verify_code());
                         params.put("email_verify_status",customerProfileModel.getEmail_verify_status());
                         params.put("auth",customerProfileModel.getAuth());
                         params.put("phone",customerProfileModel.getPhone());
                         params.put("phone_verify_status",customerProfileModel.getPhone_verify());
                         params.put("phone_verify_code",customerProfileModel.getPhone_verify_code());
                         params.put("password",customerProfileModel.getPassword());
                         params.put("register_date",customerProfileModel.getPassword());
                         params.put("customer_id",customerProfileModel.getCustomer_id());
                         params.put("place",customerProfileModel.getPlace());
                         params.put("home",customerProfileModel.getHome());
                         params.put("work",customerProfileModel.getWork());
                         return params;
                     }

                     @Override
                     protected Map<String, DataPart> getByteData() {
                         Map<String, DataPart> params = new HashMap<>();
                         // file name could found file base or direct access from real path
                         // for now just get bitmap data from ImageView
                         params.put("image", new DataPart("customer.jpg", AppHelper.getFileDataFromDrawable(getActivity(),  cus_photo.getDrawable()), "image/jpeg"));
                         return params;
                     }

                 };
                 // add the request object to the queue to be executed
                 PreferenceManager.getInstance().addToRequestQueue(req,tag);


             }


    }

    }

