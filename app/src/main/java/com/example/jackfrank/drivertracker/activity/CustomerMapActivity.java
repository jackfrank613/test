package com.example.jackfrank.drivertracker.activity;

import android.annotation.SuppressLint;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.jackfrank.drivertracker.R;
import com.example.jackfrank.drivertracker.fragment.CustomerNearbyFragment;
import com.example.jackfrank.drivertracker.fragment.CustomerOrderFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class CustomerMapActivity extends AppCompatActivity  implements OnMapReadyCallback {
    double latitude=45.66;
    double longitude=124.67;
    private ImageView s_btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Retrieve the content view that renders the map.
        setContentView(R.layout.google_map_activity);
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        s_btn=(ImageView)findViewById(R.id.float_btn);
        s_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Add a marker in Sydney, Australia,
        // and move the map's camera to the same location.

        BitmapDescriptor iconA = BitmapDescriptorFactory.fromResource(R.drawable.map_icon);
        BitmapDescriptor iconB = BitmapDescriptorFactory.fromResource(R.drawable.your_location);
        LatLng locationA = new LatLng(latitude,longitude);
        LatLng locationB= new LatLng(53,119);
        googleMap.addMarker(new MarkerOptions().position(locationA)
                .title("Package Location").icon(iconA).anchor(0.5f, 0.5f));
        googleMap.addMarker(new MarkerOptions().position(locationB)
                .title("Your Location").icon(iconB).anchor(0.5f, 0.5f));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(locationA));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(locationB));
        googleMap.animateCamera(CameraUpdateFactory.zoomIn());
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(20), 2000, null);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(locationA)      // Sets the center of the map to Mountain View
                .zoom(10)                   // Sets the zoom
                .bearing(90)                // Sets the orientation of the camera to east
                .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

    }

}
