package com.example.jackfrank.drivertracker.activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.jackfrank.drivertracker.R;
import com.example.jackfrank.drivertracker.adapter.ChatListAdapter;
import com.example.jackfrank.drivertracker.model.ChatModel;
import com.example.jackfrank.drivertracker.utils.Constants;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ChatActivity extends AppCompatActivity {
   private String TAG="chatActivity";
    private ArrayList<ChatModel> chatModels;
    private EditText et_message;
    private RecyclerView recyclerView;
    private ChatListAdapter chatListAdapter;
    private ImageView sendimage, left_arrow, gallery;
    private Constants constants;
    private Uri filePath, downloadurl;
    private final int PICK_IMAGE_REQUEST = 71;
    private String image_url;
    //Firebase
    FirebaseStorage storage;
    StorageReference ref;
    FirebaseDatabase database ;
    DatabaseReference myRef;
    private Bitmap my_image;
    private String now_time;
    String deviceToken;
    String title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_chat);
        IntentFilter intentFilter = new IntentFilter("com.it_tech_613.zhe.tamargy.message");
        LocalBroadcastManager.getInstance(this).registerReceiver(onNotice, intentFilter);
       // deviceToken="cG86_lU9Kd4:APA91bGN-uHJSJ2HY3Yy5hR6ACREi9SHlhqpLgmrtxvaaeBdnudklmYjwXGuiEcGqdsyelx9QCTPwhVauvfV1hc2Okz7l2OoA1XiWukWxzKLR16rKMyHe7kIMHRQkQ1tIZIb72yEtFGy";
        deviceToken="c4WFUC7qNwI:APA91bF73YBj9JCMN1iOs9ZuOrmIOP9QqNQFwgyd3F9bu6vPmZuZXBYWSLifPnoEUI_hfF2ZsgwnfPKDDFGGkQqt5-36bywwnwYfbIMtORwzcKPnmldpbzf33qcxc00lC2XY1fAoEf4t";
        title = "New message";
        database=FirebaseDatabase.getInstance();
        storage = FirebaseStorage.getInstance();
        ref=storage.getReference();
        chatModels=new ArrayList<ChatModel>();
        myRef = database.getReference("Message");
        initXml();
        initRead();
        sendimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendMessage();
            }
        });
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // sendmessage(deviceToken, title, "Http:\\google.com", "text");
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });
        left_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void initXml() {
        recyclerView = (RecyclerView) findViewById(R.id.chat_room);
        sendimage = (ImageView) findViewById(R.id.sendimage);
        left_arrow = (ImageView) findViewById(R.id.arrow);
        et_message = (EditText) findViewById(R.id.chat_message);
        gallery = (ImageView) findViewById(R.id.gallery_btn);

    }

    public void SendMessage() {
        //String deviceToken = "c4WFUC7qNwI:APA91bF73YBj9JCMN1iOs9ZuOrmIOP9QqNQFwgyd3F9bu6vPmZuZXBYWSLifPnoEUI_hfF2ZsgwnfPKDDFGGkQqt5-36bywwnwYfbIMtORwzcKPnmldpbzf33qcxc00lC2XY1fAoEf4t";
        String msgbody = et_message.getText().toString();
        if (!msgbody.equals("")) {
            SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss");
            String now_time = formater.format(new Date());
            sendmessage(deviceToken, title, "Http:\\google.com", msgbody,"",now_time);
            ChatModel model = new ChatModel(msgbody, now_time, "me","");
            chatModels.add(model);
            chatListAdapter = new ChatListAdapter(ChatActivity.this, chatModels);
            LinearLayoutManager manager = new LinearLayoutManager(ChatActivity.this);
            manager.setStackFromEnd(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(ChatActivity.this));
            recyclerView.setAdapter(chatListAdapter);
            recyclerView.smoothScrollToPosition(chatModels.size() - 1);
            et_message.setText("");
            HashMap<String,String>params=new HashMap<String, String>();
            params.put("message",msgbody);
            params.put("time",now_time);
            params.put("sort","me");
            params.put("Imageadress","");
            myRef=database.getReference("Message").child("1").child("2").child(now_time);
            myRef.setValue(params);

        } else {
            Toast.makeText(ChatActivity.this, "please enter the message correctly", Toast.LENGTH_SHORT).show();
        }
    }

    public void sendmessage(final String deviceToken, final String title, final String icon, final String body,final String imageurl,final String time) {


        new AsyncTask<String, String, String>() {
            @Override
            protected String doInBackground(String... params) {
                try {
                    JSONObject root = new JSONObject();
                    JSONObject notification = new JSONObject();
                    notification.put("body", body);
                    notification.put("title", title);
                    notification.put("icon", icon);

                    JSONObject data = new JSONObject();
                    data.put("image",imageurl);
                    data.put("time",time);
//
//                    JSONObject jsonObject = new JSONObject();
//                    jsonObject.put("title", message);
//                    jsonObject.put("message_type", DialogsUtils.imageUrl+userProfileImg);//19
//                    jsonObject.put("senderID", userId+"_"+chat_userId);
//                    jsonObject.put("userName", title);
//                    jsonObject.put("curretChildId", deviceID);
//
//                    data.put("message", jsonObject);
//                    data.put("curretChildId", deviceID);
                    //  data.put("message", message);

                    root.put("notification", notification); //root.put("notification", notification);
                     root.put("data", data);
                    root.put("to", deviceToken);

                    String result = postToFCM(root.toString());
                    Log.d("Main Activity", "Result: " + result);
                    return result;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                try {
                    JSONObject resultJson = new JSONObject(result);
                    if (resultJson != null) {
                        int success, failure;
                        success = resultJson.getInt("success");
                        failure = resultJson.getInt("failure");
                    } else {
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute();
    }

    String postToFCM(String bodyString) throws IOException {

        OkHttpClient mClient = new OkHttpClient();

        final MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");

        RequestBody body = RequestBody.create(JSON, bodyString);
        Request request = new Request.Builder()
                .url("https://fcm.googleapis.com/fcm/send")
                .post(body)
                .addHeader("Authorization", "key=AAAA12Avu5E:APA91bEld5_big7BLc1CN5WpaqP4tw0aawBjG3VnO7eoi3c18Pmf4z2cInQnDSreu4Je8rV1TMZLO9mcDcZV555_xPoSQLVxzGVdeerpv5Pvh5tm69B9OBfUmGXkYsJKz_UwejqWTskH")
                .build();
        Response response = mClient.newCall(request).execute();
        return response.body().string();
    }
    private BroadcastReceiver onNotice = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Update your RecyclerView here using notifyItemInserted(position);
            Bundle bundle = intent.getExtras();
            String body = bundle.getString("body");
            String time=bundle.getString("time");
            String image=bundle.getString("image");
                ChatModel model = new ChatModel(body, time, "you",image);
                chatModels.add(model);
                chatListAdapter = new ChatListAdapter(ChatActivity.this, chatModels);
                LinearLayoutManager manager = new LinearLayoutManager(ChatActivity.this);
                manager.setStackFromEnd(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(ChatActivity.this));
                recyclerView.setAdapter(chatListAdapter);
                recyclerView.smoothScrollToPosition(chatModels.size() - 1);
                    HashMap<String, String> params = new HashMap<String, String>();
                    params.put("message", body);
                    params.put("time", time);
                    params.put("sort", "you");
                    params.put("Imageadress",image);
                    myRef = database.getReference("Message").child("1").child("2").child(time);
                    myRef.setValue(params);
        }


    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
            filePath = data.getData();
            if (filePath != null) {
                final ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.setTitle("Uploading...");
                progressDialog.show();
                ref = FirebaseStorage.getInstance().getReference().child("images/" + UUID.randomUUID().toString());
                ref.putFile(filePath).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                       ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                Log.d(TAG, "onSuccess: uri= "+ uri.toString());
                                progressDialog.dismiss();
                                image_url=uri.toString();
                                SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss");
                                String nower_time = formater.format(new Date());
                                sendmessage(deviceToken, title, "Http:\\google.com", "",image_url,nower_time);
                                HashMap<String,String>params=new HashMap<String, String>();
                                params.put("message","");
                                params.put("time",nower_time);
                                params.put("sort","me");
                                params.put("Imageadress",uri.toString());
                                myRef=database.getReference("Message").child("1").child("2").child(nower_time);
                                myRef.setValue(params);
                                ChatModel model = new ChatModel("", nower_time, "me",uri.toString());
                                chatModels.add(model);
                                chatListAdapter = new ChatListAdapter(ChatActivity.this, chatModels);
                                LinearLayoutManager manager = new LinearLayoutManager(ChatActivity.this);
                                manager.setStackFromEnd(true);
                                recyclerView.setLayoutManager(new LinearLayoutManager(ChatActivity.this));
                                recyclerView.setAdapter(chatListAdapter);
                                recyclerView.smoothScrollToPosition(chatModels.size() - 1);
                            }
                        });
                    }
                });
            }
        }
    }
    public void initRead(){

        myRef.child("1").child("2").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                chatModels = new ArrayList<ChatModel>();
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    String msg=ds.child("message").getValue(String.class);
                    String time=ds.child("time").getValue(String.class);
                    String sort=ds.child("sort").getValue(String.class);
                    String imageadress=ds.child("Imageadress").getValue(String.class);
                        ChatModel model = new ChatModel(msg, time, sort, imageadress);
                        chatModels.add(model);
                        chatListAdapter = new ChatListAdapter(ChatActivity.this, chatModels);
                    LinearLayoutManager manager = new LinearLayoutManager(ChatActivity.this);
                    manager.setStackFromEnd(true);
                    recyclerView.setLayoutManager(new LinearLayoutManager(ChatActivity.this));
                    recyclerView.setAdapter(chatListAdapter);
                    recyclerView.smoothScrollToPosition(chatModels.size() - 1);
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });


    }
}

