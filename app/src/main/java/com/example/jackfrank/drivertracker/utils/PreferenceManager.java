package com.example.jackfrank.drivertracker.utils;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class PreferenceManager extends Application {

    public static final String TAG = PreferenceManager.class.getSimpleName();

    private RequestQueue mRequestQueue;

    private static PreferenceManager mInstance;

    static SharedPreferences preferences;
    static SharedPreferences.Editor prefEditor;

    public static Realm realm;

    public static String Lang = "language";
    public static String CusUserId = "CusUserId";
    public static String CliUserId="CliUserId";
    public static String LoginStatus = "LoginStatus";                           // boolean

    public static String CurrentLat = "CurrentLat";                             // double
    public static String CurrentLng = "CurrentLng";                             // double

    public static String LoginMethod="LoginMethod";
    public static String Role="Role";
    public static void resetAll() {

        prefEditor.clear();
        prefEditor.commit();

    }

    public static void clear() {

        prefEditor.clear();
    }

    @Override
    public void onCreate() {

        super.onCreate();

        mInstance = this;
        preferences = getSharedPreferences("Ogrenciyim", Context.MODE_PRIVATE);
        prefEditor = preferences.edit();

        prefEditor.commit();

        // -----------------------------------------------------------------------------------------
        Realm.init(this);
        final RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name("Flamingo.realm")
                .schemaVersion(2)
                .deleteRealmIfMigrationNeeded()
                .migration(new RealmMigrations())
                .build();

        Realm.setDefaultConfiguration(realmConfiguration);

        realm= Realm.getInstance(realmConfiguration);


    }

    public static synchronized PreferenceManager getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public void onTerminate() {
        // TODO Auto-generated method stub
        realm.close();
        realm.deleteAll();
        super.onTerminate();
    }

    public static String getLanguage() {
        return preferences.getString(Lang, "");
    }

    public static void setLanguage(String language) {
        prefEditor.putString(Lang, language);
        prefEditor.commit();
    }

    public static void setUserId(long cusUserId) {
        prefEditor.putLong(CusUserId, cusUserId);
        prefEditor.commit();
    }

    public static long getUserId() {
        return preferences.getLong(CusUserId, -1);
    }

    public static void setEmail(String email)
    {
        prefEditor.putString("email",email);
        prefEditor.commit();

    }
    public static String setCustomer(String customer)
    {
        prefEditor.putString("Customer",customer);
        prefEditor.commit();

        return customer;
    }
    public static String getCustomer(){

        return preferences.getString("Customer","");
    }


    public static String getPay(){

        return preferences.getString("pay"," ");
    }

    public static void setPay(String pay)
    {
        prefEditor.putString("pay",pay);
        prefEditor.commit();

    }
    public static void setPeriod(long period)
    {
        prefEditor.putLong("Period",period);
        prefEditor.commit();

    }
    public static long getPeriod(){

        return preferences.getLong("Period",0);
    }

    public static String getEmail(){

        return preferences.getString("email"," ");
    }
    public static void setUri(String uri)
    {
        prefEditor.putString("uri",uri);
        prefEditor.commit();
    }
    public static String getUri(){
        return preferences.getString("uri","");
    }
    public static void setFirstname(String firstname)
    {
        prefEditor.putString("firstname",firstname);
        prefEditor.commit();
    }
    public static String geFirstname(){
        return preferences.getString("firstname","");
    }
    public static void setLastname(String Lastname)
    {
        prefEditor.putString("lastname",Lastname);
        prefEditor.commit();
    }
    public static String geLastname(){
        return preferences.getString("lastname","");
    }
    public static void setStrudy(String study)
    {
        prefEditor.putString("study",study);
        prefEditor.commit();
    }
    public static String getStudy(){
        return preferences.getString("study","");
    }
    public static void setFrom(String from)
    {
        prefEditor.putString("from",from);
        prefEditor.commit();
    }
    public static String getFrom(){
        return preferences.getString("from","");
    }
    public static void setGender(String gender)
    {
        prefEditor.putString("gender",gender);
        prefEditor.commit();
    }
    public static String getGender(){
        return preferences.getString("gender","");
    }




    public static void setLoginStatus(String loginStatus) {
        prefEditor.putString(LoginStatus, loginStatus);
        prefEditor.apply();
    }

    public static String getLoginStatus() {
        return preferences.getString(LoginStatus, "");
    }

    public static void setLoginMethod(String loginMethod) {
        prefEditor.putString(LoginMethod, loginMethod);
        prefEditor.apply();
    }

    public static String getLoginMethod() {
        return preferences.getString(LoginMethod, "");
    }



//    public static void logoutManager(int role_int){
////        if (role_int==0){
////            PreferenceManager.setCusUserId(-1);
////        }else if(role_int==1){
////            PreferenceManager.setCliUserId(-1);
////        }
//        PreferenceManager.setLoginMethod("");
//        PreferenceManager.setLoginStatus(Constant.LoginStatus.UserNotSignedIn.toString());
////        PreferenceManager.setRole("");
//    }
}
