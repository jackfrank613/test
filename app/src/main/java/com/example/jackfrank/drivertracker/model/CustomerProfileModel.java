package com.example.jackfrank.drivertracker.model;

public class CustomerProfileModel {

    private String user_id;
    private String name;
    private String email;
    private String token;
    private String email_verify_code;
    private String email_verify_status;
    private String auth;
    private String phone;
    private String phone_verify;
    private String password;
    private String register_date;
    private String customer_id;
    private String place;
    private String image;
    private String home;
    private String work;



    private String phone_verify_code;

    public CustomerProfileModel(String user_id, String name, String email, String token, String email_verify_code, String email_verify_status, String auth, String phone, String phone_verify, String password, String register_date, String customer_id, String place, String image,String home, String work,String phone_verify_code) {

        this.user_id=user_id;
        this.name=name;
        this.email=email;
        this.token=token;
        this.email_verify_code=email_verify_code;
        this.email_verify_status=email_verify_status;
        this.auth=auth;
        this.phone=phone;
        this.phone_verify=phone_verify;
        this.password=password;
        this.register_date=register_date;
        this.customer_id=customer_id;
        this.place=place;
        this.image=image;
        this.home=home;
        this.work=work;
        this.phone_verify_code=phone_verify_code;
    }

    public CustomerProfileModel() {

    }
    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEmail_verify_code() {
        return email_verify_code;
    }

    public void setEmail_verify_code(String email_verify_code) {
        this.email_verify_code = email_verify_code;
    }

    public String getEmail_verify_status() {
        return email_verify_status;
    }

    public void setEmail_verify_status(String email_verify_status) {
        this.email_verify_status = email_verify_status;
    }

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone_verify() {
        return phone_verify;
    }

    public void setPhone_verify(String phone_verify) {
        this.phone_verify = phone_verify;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRegister_date() {
        return register_date;
    }

    public void setRegister_date(String register_date) {
        this.register_date = register_date;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }
    public String getPhone_verify_code() {
        return phone_verify_code;
    }

    public void setPhone_verify_code(String phone_verify_code) {
        this.phone_verify_code = phone_verify_code;
    }

}