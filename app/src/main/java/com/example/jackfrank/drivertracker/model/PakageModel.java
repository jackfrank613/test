package com.example.jackfrank.drivertracker.model;

public class PakageModel {
    private String p_id;
    private String p_name;
    private String p_location;
    private String p_image;

    public PakageModel(){}
    public PakageModel(String p_id,String p_name,String p_location,String p_image){

        this.p_id=p_id;
        this.p_name=p_name;
        this.p_image=p_image;
        this.p_location=p_location;
    }

    public String getP_id() {
        return p_id;
    }

    public void setP_id(String p_id) {
        this.p_id = p_id;
    }

    public String getP_name() {
        return p_name;
    }

    public void setP_name(String p_name) {
        this.p_name = p_name;
    }

    public String getP_location() {
        return p_location;
    }

    public void setP_location(String p_location) {
        this.p_location = p_location;
    }

    public String getP_image() {
        return p_image;
    }

    public void setP_image(String p_image) {
        this.p_image = p_image;
    }

}
