package com.example.jackfrank.drivertracker.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.example.jackfrank.drivertracker.R;
import com.example.jackfrank.drivertracker.activity.LoginActivity;

public class DriverUplodingLicenceFragment extends Fragment {

    public static DriverUplodingLicenceFragment newInstance() {
        DriverUplodingLicenceFragment fragment = new DriverUplodingLicenceFragment();
    return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frament_driver_upload_driving_license, container, false);
        ImageView leftarrow=(ImageView)view.findViewById(R.id.arrow);
        leftarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction =getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.main_content, DriverVehicleOwnershipForm.newInstance()).addToBackStack("tag").commit();
            }
        });
        Button btn_submit=(Button)view.findViewById(R.id.sub_btn);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(),LoginActivity.class);
                startActivity(intent);
            }
        });
        return view;
    }


}
