package com.example.jackfrank.drivertracker.model;

public class CustomerActiveOrderModel {
    private String d_name;
    private String d_number;
    private String d_price;
    private String p_option;
    private String order_day;
    private String order_process;

    public CustomerActiveOrderModel(){}
    public CustomerActiveOrderModel(String d_name,String d_number,String d_price,String p_option,String order_day,String order_process)
    {
        this.d_name=d_name;
        this.d_number=d_number;
        this.d_price=d_price;
        this.p_option=p_option;
        this.order_process=order_process;
        this.order_day=order_day;
    }

    public String getD_name() {
        return d_name;
    }

    public void setD_name(String d_name) {
        this.d_name = d_name;
    }

    public String getD_number() {
        return d_number;
    }

    public void setD_number(String d_number) {
        this.d_number = d_number;
    }

    public String getD_price() {
        return d_price;
    }

    public void setD_price(String d_price) {
        this.d_price = d_price;
    }

    public String getP_option() {
        return p_option;
    }

    public void setP_option(String p_option) {
        this.p_option = p_option;
    }

    public String getOrder_day() {
        return order_day;
    }

    public void setOrder_day(String order_day) {
        this.order_day = order_day;
    }

    public String getOrder_process() {
        return order_process;
    }

    public void setOrder_process(String order_process) {
        this.order_process = order_process;
    }
}
