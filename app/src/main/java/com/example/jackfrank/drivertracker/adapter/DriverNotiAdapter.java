package com.example.jackfrank.drivertracker.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.jackfrank.drivertracker.R;
import com.example.jackfrank.drivertracker.activity.ChatActivity;
import com.example.jackfrank.drivertracker.activity.CustomerMainActivity;
import com.example.jackfrank.drivertracker.activity.DriverMainActivity;
import com.example.jackfrank.drivertracker.fragment.CustomerDriverProfileFragment;
import com.example.jackfrank.drivertracker.fragment.DriverCustomerProfileFragment;
import com.example.jackfrank.drivertracker.model.CustomerNotiModel;
import com.example.jackfrank.drivertracker.model.DriverNotiModel;

import java.util.ArrayList;

public class DriverNotiAdapter extends RecyclerView.Adapter<DriverNotiAdapter.ViewHolder> {

    private Context context;
    private ArrayList<DriverNotiModel> reviewModels;
    public DriverNotiAdapter(Context context,ArrayList<DriverNotiModel> driverNotiModels) {
        this.context=context;
        this.reviewModels=driverNotiModels;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.item_driver_notification, viewGroup, false);
        return new ViewHolder(itemView);
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    @SuppressLint({"SetTextI18n", "CheckResult"})
    @Override
    public void onBindViewHolder(@NonNull DriverNotiAdapter.ViewHolder viewHolder, final int i) {
        final DriverNotiModel reviewModel = reviewModels.get(i);
        viewHolder.review.setText(reviewModel.getD_title());
        viewHolder.user_name.setText(reviewModel.getD_day());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context,"click listener",Toast.LENGTH_SHORT).show();
                fragmentJump();

            }
        });
    }

    public int getItemCount() {
        return reviewModels.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView review;
        TextView user_name;
        public ViewHolder(View view) {
            super(view);
            review=(TextView)view.findViewById(R.id.description);
            user_name=(TextView)view.findViewById(R.id.day);
        }
    }
    public void fragmentJump(){
       DriverCustomerProfileFragment orderFragment=DriverCustomerProfileFragment.newInstance();
        switchContent(R.id.cus_content,orderFragment);

    }
    public void switchContent(int id, Fragment fragment) {
        if (context == null)
            return;
        if (context instanceof DriverMainActivity) {
            DriverMainActivity mainActivity = (DriverMainActivity) context;
            Fragment frag = fragment;
            mainActivity.switchContent(id, frag);
        }
    }
}
