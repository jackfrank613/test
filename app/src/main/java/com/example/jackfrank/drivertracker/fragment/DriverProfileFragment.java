package com.example.jackfrank.drivertracker.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.jackfrank.drivertracker.R;
import com.example.jackfrank.drivertracker.bottom_nav_service.BottomNavigationViewNew;

public class DriverProfileFragment extends Fragment {

    public static DriverProfileFragment newInstance(){

        DriverProfileFragment fragment=new DriverProfileFragment();
        return fragment;

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_driver_profile, container, false);
        initXml();
        return view;
    }
    public void initXml(){
        Toolbar toolbar=(Toolbar)getActivity().findViewById(R.id.toolbar);
        toolbar.setVisibility(View.VISIBLE);
        LinearLayout layout=(LinearLayout)getActivity().findViewById(R.id.tabbar);
        layout.setVisibility(View.GONE);
        TextView title=(TextView)getActivity().findViewById(R.id.tvTitle);
        title.setText("Profile");
        ImageView imageView=(ImageView)getActivity().findViewById(R.id.imagesearch);
        imageView.setVisibility(View.GONE);
        BottomNavigationViewNew bottom=(BottomNavigationViewNew)getActivity().findViewById(R.id.navigation);
        bottom.setVisibility(View.VISIBLE);
    }

}
