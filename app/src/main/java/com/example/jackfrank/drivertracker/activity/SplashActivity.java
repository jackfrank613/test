package com.example.jackfrank.drivertracker.activity;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.app.AppComponentFactory;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.jackfrank.drivertracker.R;
import com.google.firebase.iid.FirebaseInstanceId;

public class SplashActivity extends AppCompatActivity {
    private RelativeLayout background;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initXml();
        int colorFrom = getResources().getColor(R.color.back_color);
        int colorTo = getResources().getColor(R.color.white);
        int duration = 1000;
        ObjectAnimator.ofObject(background, "backgroundColor", new ArgbEvaluator(), colorTo, colorFrom)
                .setDuration(duration)
                .start();
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(duration);
        background.startAnimation(anim);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Intent intent = new Intent(SplashActivity.this, SignupActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    public void initXml() {
        background = (RelativeLayout) findViewById(R.id.splash);

    }

}
