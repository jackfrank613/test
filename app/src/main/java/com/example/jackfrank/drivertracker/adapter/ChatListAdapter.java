package com.example.jackfrank.drivertracker.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jackfrank.drivertracker.R;
import com.example.jackfrank.drivertracker.model.ChatHistoryModel;
import com.example.jackfrank.drivertracker.model.ChatModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.MyViewHolder> {
    private List<ChatModel> moviesList;
    private Context context;
    private Boolean test;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView receive_message,receive_time,send_message,send_time;
        public ImageView profile_picture,send_picture,receive_picture;

        public MyViewHolder(View view) {
            super(view);
            receive_message = (TextView) view.findViewById(R.id.receive_message);
            receive_time= (TextView) view.findViewById(R.id.receive_time);
            send_message=(TextView)view.findViewById(R.id.send_message);
            send_time=(TextView)view.findViewById(R.id.send_time);
            profile_picture=(ImageView)view.findViewById(R.id.msgpic);
            send_picture=(ImageView)view.findViewById(R.id.send_image);
            receive_picture=(ImageView)view.findViewById(R.id.receive_image);

        }
    }
    public ChatListAdapter(Context context, List<ChatModel> moviesList) {
        this.context=context;
        this.moviesList = moviesList;
    }
    @Override
    public ChatListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_chat_process, parent, false);
        return new ChatListAdapter.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(ChatListAdapter.MyViewHolder holder, int position) {
       ChatModel movie = moviesList.get(position);
       if(movie.getSort().equals("me"))
       {
           if (movie.getMessage().equals("")){
               holder.send_time.setText(movie.getChat_time());
               //  holder.send_message.setText(movie.getMessage());
               holder.send_message.setVisibility(View.GONE);
               holder.receive_time.setVisibility(View.GONE);
               holder.receive_message.setVisibility(View.GONE);
               holder.receive_picture.setVisibility(View.GONE);
               holder.send_picture.setVisibility(View.VISIBLE);
               holder.profile_picture.setVisibility(View.GONE);
              Picasso.with(context).load(movie.getImg_url()).placeholder(R.drawable.phone).into(holder.send_picture);
           }

          else  {
               holder.send_time.setText(movie.getChat_time());
               holder.send_message.setText(movie.getMessage());
               holder.send_message.setVisibility(View.VISIBLE);
               holder.receive_time.setVisibility(View.GONE);
               holder.receive_message.setVisibility(View.GONE);
               holder.receive_picture.setVisibility(View.GONE);
               holder.send_picture.setVisibility(View.GONE);
               holder.profile_picture.setVisibility(View.GONE);
           }
       }
   else {
           if(movie.getMessage().equals("")) {
               holder.receive_time.setText(movie.getChat_time());
               holder.receive_message.setVisibility(View.GONE);
               holder.send_message.setVisibility(View.GONE);
               holder.send_picture.setVisibility(View.GONE);
               holder.send_time.setVisibility(View.GONE);
               holder.receive_picture.setVisibility(View.VISIBLE);
               Picasso.with(context).load(movie.getImg_url()).placeholder(R.drawable.phone).into(holder.receive_picture);
           }
           else {
               holder.receive_time.setText(movie.getChat_time());
               holder.receive_message.setText(movie.getMessage());
               holder.receive_message.setVisibility(View.VISIBLE);
               holder.send_message.setVisibility(View.GONE);
               holder.receive_picture.setVisibility(View.GONE);
               holder.send_picture.setVisibility(View.GONE);
               holder.send_time.setVisibility(View.GONE);
           }
       }

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
