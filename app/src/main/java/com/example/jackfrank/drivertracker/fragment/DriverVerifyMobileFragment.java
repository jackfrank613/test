package com.example.jackfrank.drivertracker.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.example.jackfrank.drivertracker.R;

public class DriverVerifyMobileFragment extends Fragment {

    public static DriverVerifyMobileFragment newInstance(){

        DriverVerifyMobileFragment fragment=new DriverVerifyMobileFragment();
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_driver_verify_mobile, container, false);
        ImageView leftarrow=(ImageView)view.findViewById(R.id.leftarrow);
        leftarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction =getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.main_content, DriverVerifyEmailFragment.newInstance()).addToBackStack("tag").commit();
            }
        });
        Button btn_submit=(Button)view.findViewById(R.id.sub_btn);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction =getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.main_content, DriverAddVehicleDetailFragment.newInstance()).addToBackStack("tag").commit();
            }
        });
        return view;
    }
}
