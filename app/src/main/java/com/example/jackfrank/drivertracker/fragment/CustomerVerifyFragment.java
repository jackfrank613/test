package com.example.jackfrank.drivertracker.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.jackfrank.drivertracker.R;
import com.example.jackfrank.drivertracker.activity.CustomerMainActivity;
import com.example.jackfrank.drivertracker.activity.LoginActivity;
import com.example.jackfrank.drivertracker.activity.SignupActivity;

public class CustomerVerifyFragment extends Fragment {
    private Button btn;
    private EditText v_code;
    private Button btn_change,btn_resend;
    private ImageView arrow;
    public static CustomerVerifyFragment newInstance() {
        CustomerVerifyFragment fragment = new CustomerVerifyFragment();
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_veryfiy, container, false);
        btn=(Button)view.findViewById(R.id.sub_btn);
        v_code=(EditText)view.findViewById(R.id.et_vcode);
        btn_change=(Button)view.findViewById(R.id.change);
        btn_resend=(Button)view.findViewById(R.id.resend);
        arrow=(ImageView)view.findViewById(R.id.leftarrow);
        arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction =getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.content, CustomerPersonalInformation.newInstance()).addToBackStack("tag").commit();
            }
        });
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(),CustomerMainActivity.class);
                startActivity(intent);
            }
        });
        return view;
    }

}
