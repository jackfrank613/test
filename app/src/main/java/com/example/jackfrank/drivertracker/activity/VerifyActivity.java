package com.example.jackfrank.drivertracker.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.example.jackfrank.drivertracker.R;
import com.example.jackfrank.drivertracker.fragment.CustomerPersonalInformation;
import com.example.jackfrank.drivertracker.fragment.CustomerVerifyFragment;

public class VerifyActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content, CustomerPersonalInformation.newInstance()).addToBackStack("tag").commit();
    }
}
