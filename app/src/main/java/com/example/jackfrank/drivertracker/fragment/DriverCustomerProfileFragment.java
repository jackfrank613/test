package com.example.jackfrank.drivertracker.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.jackfrank.drivertracker.R;
import com.example.jackfrank.drivertracker.activity.ChatActivity;
import com.example.jackfrank.drivertracker.bottom_nav_service.BottomNavigationViewNew;

public class DriverCustomerProfileFragment extends Fragment {
    public static DriverCustomerProfileFragment newInstance(){

        DriverCustomerProfileFragment fragment=new DriverCustomerProfileFragment();
        return  fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_driver_customer_profile, container, false);
        initXml();
        ImageView rate_arrow=(ImageView)view.findViewById(R.id.rateid) ;
        ImageView left_arrow=(ImageView)view.findViewById(R.id.noti_arrow) ;
        ImageView chat_arrow=(ImageView)view.findViewById(R.id.chatid) ;
        rate_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.cus_content, DriverCustomerRateNow.newInstance()).addToBackStack("tag").commit();
            }
        });
        left_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.cus_content, DriverNotiFragment.newInstance()).addToBackStack("tag").commit();
            }
        });
        chat_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent intent=new Intent(getActivity(),ChatActivity.class);
               startActivity(intent);
            }
        });
        return view;
    }
    public void  initXml(){
        Toolbar toolbar = (Toolbar)getActivity().findViewById(R.id.toolbar);
        toolbar.setVisibility(View.GONE);
        LinearLayout layout=(LinearLayout)getActivity().findViewById(R.id.tabbar);
        layout.setVisibility(View.GONE);
        BottomNavigationViewNew bottom=(BottomNavigationViewNew)getActivity().findViewById(R.id.navigation);
        bottom.setVisibility(View.GONE);
    }
}
