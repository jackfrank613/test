package com.example.jackfrank.drivertracker.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.example.jackfrank.drivertracker.R;
import com.example.jackfrank.drivertracker.activity.SignupActivity;

public class DriverVerifyEmailFragment extends Fragment {
    public static DriverVerifyEmailFragment newInstance(){
        DriverVerifyEmailFragment fragment=new DriverVerifyEmailFragment();
        return fragment;

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_driver_verify_email, container, false);
        ImageView leftarrow=(ImageView)view.findViewById(R.id.leftarrow);
        leftarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction =getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.main_content, DriverPersonalInformation.newInstance()).addToBackStack("tag").commit();
            }
        });
        Button btn_submit=(Button)view.findViewById(R.id.sub_btn);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction =getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.main_content, DriverVerifyMobileFragment.newInstance()).addToBackStack("tag").commit();
            }
        });
        return view;
    }
}
