package com.example.jackfrank.drivertracker.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.jackfrank.drivertracker.R;
import com.example.jackfrank.drivertracker.model.CustomerNotiModel;

import java.util.ArrayList;
public class CustomerNotiAdapter extends RecyclerView.Adapter<CustomerNotiAdapter.ViewHolder>{
    private Context context;
    private ArrayList<CustomerNotiModel> reviewModels;
    public CustomerNotiAdapter(Context context,ArrayList<CustomerNotiModel> customerNotiModels) {
        this.context=context;
        this.reviewModels=customerNotiModels;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.item_cutomer_notification, viewGroup, false);
        return new ViewHolder(itemView);
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    @SuppressLint({"SetTextI18n", "CheckResult"})
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        final CustomerNotiModel reviewModel = reviewModels.get(i);
        viewHolder.review.setText(reviewModel.getTitle());
        viewHolder.user_name.setText(reviewModel.getDay());
    }

    public int getItemCount() {
        return reviewModels.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView review;
        TextView user_name;
        public ViewHolder(View view) {
            super(view);
            review=(TextView)view.findViewById(R.id.description);
            user_name=(TextView)view.findViewById(R.id.day);
        }
    }

}
