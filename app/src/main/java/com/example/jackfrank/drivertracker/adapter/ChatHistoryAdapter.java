package com.example.jackfrank.drivertracker.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.jackfrank.drivertracker.R;
import com.example.jackfrank.drivertracker.activity.ChatActivity;
import com.example.jackfrank.drivertracker.activity.CustomerMainActivity;
import com.example.jackfrank.drivertracker.activity.DriverMainActivity;
import com.example.jackfrank.drivertracker.fragment.ChatHistoryFragment;
import com.example.jackfrank.drivertracker.fragment.CustomerOrderFragment;
import com.example.jackfrank.drivertracker.model.ChatHistoryModel;
import com.example.jackfrank.drivertracker.model.CustomerHomeSearchModel;

import java.util.List;

public class ChatHistoryAdapter extends RecyclerView.Adapter<ChatHistoryAdapter.MyViewHolder> {

    private List<ChatHistoryModel> moviesList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name,last_message,time;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            last_message= (TextView) view.findViewById(R.id.last_msg);
            time= (TextView) view.findViewById(R.id.last_time);
        }
    }

    public ChatHistoryAdapter(Context context, List<ChatHistoryModel> moviesList) {
        this.context=context;
        this.moviesList = moviesList;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_chat_history, parent, false);
        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(ChatHistoryAdapter.MyViewHolder holder, int position) {
       ChatHistoryModel movie = moviesList.get(position);
        holder.name.setText(movie.getName());
        holder.last_message.setText(movie.getLast_message());
        holder.time.setText(movie.getTime());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context,"click listener",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context, ChatActivity.class);
                context.startActivity(intent);
          //      fragmentJump();

            }
        });
    }
//    public void fragmentJump(){
//        ChatFragment orderFragment=ChatFragment.newInstance();
//      //  switchContent(R.id.cus_content,orderFragment);
//
//    }
//    public void switchContent(int id, Fragment fragment) {
//        if (context == null)
//            return;
//        if (context instanceof DriverMainActivity) {
//           DriverMainActivity mainActivity = (DriverMainActivity) context;
//            Fragment frag = fragment;
//            mainActivity.switchContent(id, frag);
//        }
//    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }


}
