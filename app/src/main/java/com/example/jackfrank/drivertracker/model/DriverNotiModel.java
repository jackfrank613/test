package com.example.jackfrank.drivertracker.model;

public class DriverNotiModel {
    private String d_title, d_day;
    public DriverNotiModel() {
    }
    public DriverNotiModel(String d_title,String d_day) {
        this.d_title=d_title;
        this.d_day=d_day;
    }
    public String getD_title() {
        return d_title;
    }

    public void setD_title(String d_title) {
        this.d_title = d_title;
    }

    public String getD_day() {
        return d_day;
    }

    public void setD_day(String d_day) {
        this.d_day = d_day;
    }

}
