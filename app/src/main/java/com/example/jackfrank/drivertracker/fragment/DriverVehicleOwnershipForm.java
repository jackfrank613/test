package com.example.jackfrank.drivertracker.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.example.jackfrank.drivertracker.R;

public class DriverVehicleOwnershipForm extends Fragment {

    public static DriverVehicleOwnershipForm newInstance() {

        DriverVehicleOwnershipForm fragment = new DriverVehicleOwnershipForm();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_driver_vehicle_ownership_form, container, false);
        ImageView leftarrow = (ImageView) view.findViewById(R.id.leftarrow);
        leftarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.main_content, DriverSelectVehicleTypeFragment.newInstance()).addToBackStack("tag").commit();
            }
        });
        Button btn_submit = (Button) view.findViewById(R.id.sub_btn);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.main_content, DriverUplodingLicenceFragment.newInstance()).addToBackStack("tag").commit();
            }
        });
        return view;
    }
}
